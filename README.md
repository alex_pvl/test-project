# README #

A reworked version of my project (NetBeans IDE).

I stayed with JXL for now to deliver faster.
-XML deserialization and serialization implemented via native JAXB.
-Serialization to JSON implemented via Moxy implementation of JAXB.

Path to output XLS file may be set at line 36 of Main.class (by default D:\rates.xls)
Required libraries (eclipselink-2.6.2.jar, org.eclipse.persistence.moxy-2.6.2.jar, validation-api-1.1.0.Final.jar, \jexcelapi\jxl.jar) are in libs folder,

I would gladly study further during internship, adapt to your styles and best practices, work hard and learn everything that will be required!

Thank you for your time!